#+TITLE: Café Rösel
#+html_head_extra: <link rel="favicon" type="image/x-icon" href="favicon.png"/>
#+OPTIONS: num:nil timestamp:nil toc:nil author:nil
#+HTML_HEAD_EXTRA: <link rel="stylesheet" href="extra.css" />


| [[./survey.html][Take our Survey!]] | Reservation [soon] | Crowdfunding [soon] |

At our cafe we provide a *comfortable and innovative study area* with nothing but the best equipment for students including /fish tanks, individual study areas, communal space for more relaxed studying/. We will  eliminate closing times being *open 24/7* for our customers. We are offering a reliable place to study adapted to all types of students: night owls and early birds; where they will find the resources they need at any given point.

* Prices
| First Entery | 1 €   |
| 2 Hours      | 1.6 € |
| 1 Day        | 4 €   |
| 1 Week       | 12 €  |
| 1 Month      | 30 €  |

* Our Story
We are students.

And as students in Segovia, we understand the struggle of finding a place to study in Segovia. We tried different study places: coffee shops, libraries, student residences, our homes. Some were better than others, but all had a major flaw. Public / school library and residences are among the most popular options, greatly due to the low standards that exist in the market in Segovia. Libraries are good for studying but closing times can cause problems, residences can be amazing to study in but are only accessible to those who live there. When it comes to studying at home, many people have a hard time focusing, as it can be a very distracting environment. We came to the conclusion that no place in Segovia is able to check all the boxes for student satisfaction.

Segovia is increasingly gaining more students every year and we believe that this city deserves a higher standard of education by having a place for its students.
